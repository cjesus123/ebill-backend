package ar.edu.undec.adapter.data.company.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "company")
public class CompanyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String reference;

    public CompanyEntity() {
    }

    public CompanyEntity(Integer id, String name, String reference) {
        this.id = id;
        this.name = name;
        this.reference = reference;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getReference() {
        return reference;
    }
}
