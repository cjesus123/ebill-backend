package ar.edu.undec.adapter.data.company.mapper;

import ar.edu.undec.adapter.data.company.exception.CompanyMapperException;
import ar.edu.undec.adapter.data.company.model.CompanyEntity;
import edu.undec.ebill.company.model.Company;

public class CompanyDataMapper {

    public static Company dataCoreMapper(CompanyEntity companyEntity) {
        try {
            return Company.instanceCore(companyEntity.getId(),
                    companyEntity.getName(),
                    companyEntity.getReference());
        } catch (Exception e) {
            throw new CompanyMapperException("Error map Entity to Core");
        }
    }

    public static CompanyEntity dataEntityMapper(Company company) {
        try {
            return new CompanyEntity(company.getId(),
                    company.getName(),
                    company.getReference());
        } catch (Exception e) {
            throw new CompanyMapperException("Error map Entity to Core");
        }
    }
}
