package edu.undec.ebill.factory;

import edu.undec.ebill.company.usecase.createcompanyusecase.CreateCompanyRequestModel;

public class CompanyFactory {
    public static CreateCompanyRequestModel createCompanyRequestModel() {
        CreateCompanyRequestModel createCompanyRequestModel = CreateCompanyRequestModel.createInstance("Claro", "ABC123");
        return createCompanyRequestModel;
    }
}
