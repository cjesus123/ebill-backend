package ar.edu.undec.adapter.data.company.repoimplementation;

import ar.edu.undec.adapter.data.company.crud.CreateCompanyCRUD;
import ar.edu.undec.adapter.data.company.mapper.CompanyDataMapper;
import edu.undec.ebill.company.model.Company;
import edu.undec.ebill.company.output.CreateCompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateCompanyRepoImplementation implements CreateCompanyRepository {

    CreateCompanyCRUD createCompanyCRUD;

    @Autowired
    public CreateCompanyRepoImplementation(CreateCompanyCRUD createCompanyCRUD) {
        this.createCompanyCRUD = createCompanyCRUD;
    }

    @Override
    public boolean existByName(String name) {
        return createCompanyCRUD.existsByName(name);
    }

    @Override
    public Integer saveCompany(Company company) {
        try {
            return createCompanyCRUD.save(CompanyDataMapper.dataEntityMapper(company)).getId();
        } catch (Exception e) {
            return 0;
        }
    }
}
