package ar.edu.undec.adapter.data.company.exception;

public class CompanyMapperException extends RuntimeException {
    public CompanyMapperException(String message) {
        super(message);
    }
}
