package ar.edu.undec.adapter.data.company.crud;

import ar.edu.undec.adapter.data.company.model.CompanyEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CreateCompanyCRUD extends CrudRepository<CompanyEntity, Integer> {

    Boolean existsByName(String name);

}
