package edu.undec.ebill.company.usecase.createcompanyusecase;

import edu.undec.ebill.company.exception.CompanyExistException;
import edu.undec.ebill.company.input.CreateCompanyInput;
import edu.undec.ebill.company.model.Company;
import edu.undec.ebill.company.output.CreateCompanyRepository;

public class CreateCompanyUseCase implements CreateCompanyInput {

    CreateCompanyRepository createCompanyRepository;

    public CreateCompanyUseCase(CreateCompanyRepository createCompanyRepository) {
        this.createCompanyRepository = createCompanyRepository;
    }

    @Override
    public Integer createCompany(CreateCompanyRequestModel createCompanyRequestModel) {

        if (createCompanyRepository.existByName(createCompanyRequestModel.getName())) {
            throw new CompanyExistException("Company already exist");
        }

        Company company = Company.instanceCore(null,
                createCompanyRequestModel.getName(),
                createCompanyRequestModel.getReference());
        return createCompanyRepository.saveCompany(company);
    }
}
