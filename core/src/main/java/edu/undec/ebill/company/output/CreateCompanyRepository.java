package edu.undec.ebill.company.output;

import edu.undec.ebill.company.model.Company;

public interface CreateCompanyRepository {

    boolean existByName(String name);

    Integer saveCompany(Company company);
}
