package edu.undec.ebill.company.exception;

public class CompanyExistException extends RuntimeException {
    public CompanyExistException(String message) {
        super(message);
    }
}
