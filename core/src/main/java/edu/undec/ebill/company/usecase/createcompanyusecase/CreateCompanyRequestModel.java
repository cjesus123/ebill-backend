package edu.undec.ebill.company.usecase.createcompanyusecase;

public class CreateCompanyRequestModel {
    private String name;
    private String reference;

    private CreateCompanyRequestModel(String name, String reference) {
        this.name = name;
        this.reference = reference;
    }

    public static CreateCompanyRequestModel createInstance(String name, String reference) {
        CreateCompanyRequestModel createCompanyRequestModel = new CreateCompanyRequestModel(name, reference);
        return createCompanyRequestModel;
    }

    public String getName() {
        return name;
    }

    public String getReference() {
        return reference;
    }
}
