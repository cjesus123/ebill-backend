package edu.undec.ebill.usecase;

import edu.undec.ebill.company.input.CreateCompanyInput;
import edu.undec.ebill.company.model.Company;
import edu.undec.ebill.company.output.CreateCompanyRepository;
import edu.undec.ebill.company.usecase.createcompanyusecase.CreateCompanyRequestModel;
import edu.undec.ebill.company.usecase.createcompanyusecase.CreateCompanyUseCase;
import edu.undec.ebill.factory.CompanyFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CreateCompanyUseCaseUnitTest {

    CreateCompanyInput createCompanyInput;

    @Mock
    CreateCompanyRepository createCompanyRepository;

    @BeforeEach
    void setup() {
        this.createCompanyInput = new CreateCompanyUseCase(createCompanyRepository);
    }

    @Test
    void createCompany_CompanyNotExist_CreateCompany() {
        CreateCompanyRequestModel createCompanyRequestModel = CompanyFactory.createCompanyRequestModel();

        when(createCompanyRepository.existByName("Claro")).thenReturn(false);
        when(createCompanyRepository.saveCompany(any(Company.class))).thenReturn(1);

        Assertions.assertEquals(1, createCompanyInput.createCompany(createCompanyRequestModel));
    }
}
