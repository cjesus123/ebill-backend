package edu.undec.ebill.company.input;

import edu.undec.ebill.company.usecase.createcompanyusecase.CreateCompanyRequestModel;

public interface CreateCompanyInput {

    Integer createCompany(CreateCompanyRequestModel createCompanyRequestModel);
}
