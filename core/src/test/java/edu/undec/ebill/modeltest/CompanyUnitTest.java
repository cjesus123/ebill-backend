package edu.undec.ebill.modeltest;

import edu.undec.ebill.company.exception.CompanyIncompleteException;
import edu.undec.ebill.company.model.Company;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CompanyUnitTest {

    @Test
    public void createInstance_fullAttributes_Instance() {
        Company company = Company.instanceCore(1, "IpTel", "AAA-100");
        Assertions.assertNotNull(company);
    }

    @Test
    public void createInstance_incompleteAttributes_CompanyIncompleteException() {
        Exception exceptionBlank = Assertions.assertThrows(CompanyIncompleteException.class, () -> Company.instanceCore(1, " ", "NNN-500"));
        Exception exceptionNull = Assertions.assertThrows(CompanyIncompleteException.class, () -> Company.instanceCore(1, "Personal", null));
        Assertions.assertEquals("Name can not be null/blank", exceptionBlank.getMessage());
        Assertions.assertEquals("Reference can not be null/blank", exceptionNull.getMessage());
    }
}
