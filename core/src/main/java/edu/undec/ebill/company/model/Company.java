package edu.undec.ebill.company.model;

import edu.undec.ebill.company.exception.CompanyIncompleteException;

public class Company {

    private Integer id;
    private String name;
    private String reference;

    private Company(Integer id, String name, String reference) {
        this.id = id;
        this.name = name;
        this.reference = reference;
    }

    public static Company instanceCore(Integer id, String name, String reference) {
        if (name == null || name.isBlank()) {
            throw new CompanyIncompleteException("Name can not be null/blank");
        }
        if (reference == null || reference.isBlank()) {
            throw new CompanyIncompleteException("Reference can not be null/blank");
        }
        return new Company(id, name, reference);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getReference() {
        return reference;
    }
}
